# AKQA .Net Technical Challenge

This project contains solution to split an input text value to string as Person Name and Number to words.
The result present in one html with javascript that consume an API.

Project structure:

* API

* Application

* Presentation

* Test


---

## API

This is the API. There are 2 controllers to solve 2 things;

* Numbers to words

* Find person name


Project: AKQA.TechChallenge.API

Target Framework: .NET Core 2.1


Dependency injection configured in Startup.cs
More information:

[Dependency Injection .Net Core](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-2.1)

---

## Application

This layer responsible for Service classes.


Project: AKQA.TechChallenge.Application.Services

Target Framework: .NET Core 2.1


---

## Presentation

This layer responsible for web application front end. 
There is only 1 project under this layer. 


Project: AKQA.TechChallenge.Web

Target framework: .Net Framework 4.6.2

Front end library: Jquery, bootstrap


---

## Test

The test project built using Xunit that cover 2 services


Project: AKQA.TechChallenge.Test

Target Framework: .NET Core 2.1


---


## How to build/run this Web app and API?

1. Clone the repository 

2. Make sure .Net Core 2.1 installed

3. Build the solution

4. Hit F5 or click Start in your Visual Studio to run both API and Web app


---