﻿namespace AKQA.TechChallenge.Application.Services
{
    public interface IPersonNameFinderService
    {
        string FindPersonNameInText(string text);
    }
}
