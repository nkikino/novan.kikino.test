﻿
namespace AKQA.TechChallenge.Application.Services
{
    public interface INumberToWordService
    {
        string ConvertNumberToWords(double number);
    }
}
