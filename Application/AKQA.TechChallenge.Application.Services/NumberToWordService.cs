﻿using System;

namespace AKQA.TechChallenge.Application.Services
{
    public class NumberToWordService : INumberToWordService
    {
        private readonly string[] _unitsMap = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
        private readonly string[] _tensMap = { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

        public string ConvertNumberToWords(double number)
        {
            var beforeFloatingPointNumber = (long)Math.Floor(number);
            var beforeFloatingPointWord = string.Empty;
            if (beforeFloatingPointNumber != 0)
            {
                if(number < 999999999999)
                {
                    beforeFloatingPointWord = $"{FormatNumberToWords(beforeFloatingPointNumber)} dollars";

                    var afterFloatingPointNumber = (long)((Math.Round((number - beforeFloatingPointNumber) * 100, 2)));
                    if (afterFloatingPointNumber > 0)
                    {
                        var afterFloatingPointWord = $"{FormatSmallNumberToWord(afterFloatingPointNumber, string.Empty)} cents";

                        return $"{beforeFloatingPointWord} and {afterFloatingPointWord}";
                    }
                }
                else
                {
                    return "Maximum number is hundred billion";
                }
            }

            return beforeFloatingPointWord;
        }

        private string FormatNumberToWords(long number)
        {
            if (number == 0)
            {
                return "zero";
            }

            if (number < 0)
            {
                return $"minus {FormatNumberToWords(Math.Abs(number))}";
            }

            var words = "";

            var numberInBillion = number / 1000000000;
            var isBillion = numberInBillion > 0;
            if (isBillion)
            {
                words += $"{FormatNumberToWords(numberInBillion)} billion ";
                number %= 1000000000;
            }

            var numberInMillion = number / 1000000;
            var isMillion = numberInMillion > 0;
            if (isMillion)
            {
                words += $"{FormatNumberToWords(numberInMillion)} million ";
                number %= 1000000;
            }

            var numberInThousand = number / 1000;
            var isThousand = numberInThousand > 0;
            if (isThousand)
            {
                words += $"{FormatNumberToWords(numberInThousand)} thousand ";
                number %= 1000;
            }

            var numberInHundred = number / 100;
            var isHundred = numberInHundred > 0;
            if (isHundred)
            {
                words += $"{FormatNumberToWords(numberInHundred)} hundred ";
                number %= 100;

                var nextValue = FormatSmallNumberToWord(number, words);
                if (!nextValue.Equals(words))
                {
                    
                    words += "and";
                }
            }

            var lastValue = FormatSmallNumberToWord(number, words);
            if (lastValue.Equals(words))
            {
                words = words.TrimEnd();
            }

            words = FormatSmallNumberToWord(number, words);

            return words;
        }

        private string FormatSmallNumberToWord(long number, string words)
        {
            if (number > 0)
            {
                if (words != string.Empty)
                {
                    words += " ";
                }

                if (number < 20)
                {
                    words += _unitsMap[number];
                }
                else
                {
                    var tensMapIndex = number / 10;
                    words += _tensMap[tensMapIndex];

                    var unitsMapIndex = number % 10;
                    if (unitsMapIndex > 0)
                    {
                        words += $"-{_unitsMap[unitsMapIndex]}";
                    }
                }
            }

            return words;
        }

    }
}
