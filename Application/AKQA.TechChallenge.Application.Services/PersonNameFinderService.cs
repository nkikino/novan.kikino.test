﻿using System.Text.RegularExpressions;

namespace AKQA.TechChallenge.Application.Services
{
    public class PersonNameFinderService : IPersonNameFinderService
    {
        public string FindPersonNameInText(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                var personName = Regex.Replace(text, @"[^a-zA-Z ]+", string.Empty);
                if (!string.IsNullOrEmpty(personName))
                {
                    return personName.TrimEnd();
                }
            }

            return string.Empty;
        }
    }
}
