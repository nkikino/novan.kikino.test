﻿(function ($J, window, document) {
    $J(function () {
        var defaultFuntions = {
            init: function () {
                this.initPersonNameFinderAndNumberSpelling();
            },

            initPersonNameFinderAndNumberSpelling: function () {

                $J("#btn-submit").click(function () {
                    var textInput = $J("#text-input-to-detect").val();
                    if (textInput && textInput.length > 0) {
                        defaultFuntions.detectPersonName(textInput);
                        defaultFuntions.spellNumber(textInput);
                        $J('#result-container').removeClass('d-none');
                    }
                });
            },

            detectPersonName: function (textInput) {
                var apiUrl = "http://localhost:57348/api/PersonNameDetection/" + textInput;
                $J.ajax({
                    cache: false,
                    url: apiUrl,
                    success: function (result) {
                        if (result && result.length > 0) {
                            $J(".person-name").html(result);
                        }
                        else {
                            $J(".person-name").html("Person name is not found.");
                        }
                    }
                });
            },

            spellNumber: function (textInput) {
                var number = Number(textInput.replace(/[^0-9\.]+/g, ""));
                var apiUrl = "http://localhost:57348/api/NumberSpeller/" + number;
                $J.ajax({
                    cache: false,
                    url: apiUrl,
                    success: function (result) {
                        if (result && result.length) {
                            $J(".number-spell").html(result);
                        }
                        else {
                            $J(".number-spell").html('Unable to find number to spell.');
                        }
                    }
                });
            }
        };

        $J(document).ready(function () {
            defaultFuntions.init();
        });

    });
}(jQuery, window, document));
