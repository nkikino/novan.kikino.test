using System;
using AKQA.TechChallenge.Application.Services;
using Xunit;

namespace AKQA.TechChallenge.Test
{
    public class NumberSpellerTest
    {
        readonly NumberToWordService _numberToWordService = new NumberToWordService();

        [Fact]
        public void NoNumberToSpellTestCase()
        {
            var numberInWords = _numberToWordService.ConvertNumberToWords(0.00);

            Assert.Equal(string.Empty, numberInWords);
        }

        [Fact]
        public void NoFloatingNumberAfterPointTestCase()
        {
            var numberInWords = _numberToWordService.ConvertNumberToWords(1500);

            Assert.Equal("one thousand five hundred dollars", numberInWords);
        }

        [Fact]
        public void HundredWithAndTestCase()
        {
            var numberInWords = _numberToWordService.ConvertNumberToWords(5650);

            Assert.Equal("five thousand six hundred and fifty dollars", numberInWords);
        }

        [Fact]
        public void TwoDigitTestCase()
        {
            var numberInWords = _numberToWordService.ConvertNumberToWords(85);

            Assert.Equal("eighty-five dollars", numberInWords);
        }

        [Fact]
        public void TwoDigitWithCentsTestCase()
        {
            var numberInWords = _numberToWordService.ConvertNumberToWords(85.99);

            Assert.Equal("eighty-five dollars and ninety-nine cents", numberInWords);
        }

        [Fact]
        public void ThousandTestCase()
        {
            var numberInWords = _numberToWordService.ConvertNumberToWords(5000);

            Assert.Equal("five thousand dollars", numberInWords);
        }

        [Fact]
        public void HundredThousandTestCase()
        {
            var numberInWords = _numberToWordService.ConvertNumberToWords(500000);

            Assert.Equal("five hundred thousand dollars", numberInWords);
        }

        [Fact]
        public void MillionTestCase()
        {
            var numberInWords = _numberToWordService.ConvertNumberToWords(5000000);

            Assert.Equal("five million dollars", numberInWords);
        }

        [Fact]
        public void HundredMillionTestCase()
        {
            var numberInWords = _numberToWordService.ConvertNumberToWords(500000000);

            Assert.Equal("five hundred million dollars", numberInWords);
        }

        [Fact]
        public void BillionTestCase()
        {
            var numberInWords = _numberToWordService.ConvertNumberToWords(5000000000);

            Assert.Equal("five billion dollars", numberInWords);
        }

        [Fact]
        public void HundredBillionTestCase()
        {
            var numberInWords = _numberToWordService.ConvertNumberToWords(500000000000);

            Assert.Equal("five hundred billion dollars", numberInWords);
        }

        [Fact]
        public void HundredWithAndPlusCentsTestCase()
        {
            var numberInWords = _numberToWordService.ConvertNumberToWords(5650.55);

            Assert.Equal("five thousand six hundred and fifty dollars and fifty-five cents", numberInWords);
        }

        [Fact]
        public void OneDigitCentTestCase()
        {
            var numberInWords = _numberToWordService.ConvertNumberToWords(5650.5);

            Assert.Equal("five thousand six hundred and fifty dollars and fifty cents", numberInWords);
        }

        [Fact]
        public void TrilionTestCase()
        {
            var numberInWords = _numberToWordService.ConvertNumberToWords(5000000000000000);

            Assert.Equal("Maximum number is hundred billion", numberInWords);
        }
    }
}
