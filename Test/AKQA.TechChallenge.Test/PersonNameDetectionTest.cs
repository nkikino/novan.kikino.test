using AKQA.TechChallenge.Application.Services;
using Xunit;

namespace AKQA.TechChallenge.Test
{
    public class PersonNameDetectionTest
    {
        readonly PersonNameFinderService _personNameFinderService = new PersonNameFinderService();

        [Fact]
        public void EmptyInputTestCase()
        {
            var personName = _personNameFinderService.FindPersonNameInText(string.Empty);

            Assert.Equal(string.Empty, personName);
        }

        [Fact]
        public void MultipleEmptyCharactersAsInputTestCase()
        {
            var personName = _personNameFinderService.FindPersonNameInText("           ");

            Assert.Equal(string.Empty, personName);
        }

        [Fact]
        public void InputContainsSpaceTestCase()
        {
            var personName = _personNameFinderService.FindPersonNameInText("Novan Kikino");

            Assert.Equal("Novan Kikino", personName);
        }

        [Fact]
        public void InputContainsNumbersTestCase()
        {
            var personName = _personNameFinderService.FindPersonNameInText("Novan Kikino 123456789");

            Assert.Equal("Novan Kikino", personName);
        }

        [Fact]
        public void InputContainsSpecialCharactersTestCase()
        {
            var personName = _personNameFinderService.FindPersonNameInText("Novan Kikino #2@%&$*$*$*@(@(@");

            Assert.Equal("Novan Kikino", personName);
        }
    }
}
