﻿using AKQA.TechChallenge.Application.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace AKQA.TechChallenge.API.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AkQATechChallengeApiAllowPolicy")]
    [ApiController]
    public class PersonNameDetectionController : ControllerBase
    {
        private readonly IPersonNameFinderService _personNameFinderService;

        public PersonNameDetectionController(IPersonNameFinderService personNameFinderService)
        {
            _personNameFinderService = personNameFinderService;
        }

        // POST api/values
        [HttpGet("{textInput}")]
        public ActionResult<string> Get(string textInput)
        {
            return _personNameFinderService.FindPersonNameInText(textInput);
        }
    }
}
