﻿using AKQA.TechChallenge.Application.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace AKQA.TechChallenge.API.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AkQATechChallengeApiAllowPolicy")]
    [ApiController]
    public class NumberSpellerController : ControllerBase
    {
        private readonly INumberToWordService _numberToWordService;

        public NumberSpellerController(INumberToWordService numberToWordService)
        {
            _numberToWordService = numberToWordService;
        }

        // POST api/values
        [HttpGet("{number}")]
        public ActionResult<string> Get(double number)
        {
            return _numberToWordService.ConvertNumberToWords(number);
        }
    }
}
